<?php
    
    class Academicyear_Model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }
        public function getAcademicYears()
        {
            $result = $this->db->get('`academicyear`');
            return $result->result_array();
        }

        public function addAcademicYear($data)
        {
            $this->db->insert('academicyear', $data);
            return  $this->db->affected_rows() > 0;
        }
        public function editAcademicYear($data,$ID)
        {
            $this->db->where('ID', $ID);
            $this->db->update('academicyear', $data);
            return  $this->db->affected_rows() > 0;
        }

        public function deleteAcademicYear($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('academicyear');
            return  $this->db->affected_rows() > 0;
        }

        public function getAY($id)
        {
            $this->db->select('*');
            $this->db->where('`ID`', $id);
            $query = $this->db->get('`academicyear`');

            return $query->result_array();

        }

        public function checkDuplicate($data)
        {
            $this->db->where($data);
            $this->db->from('academicyear');
            $count = $this->db->count_all_results();
            return ($count == 0) ? false:true;
            
            
        }
    }

?>