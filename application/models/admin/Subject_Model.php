<?php
    
    class Subject_Model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }
        public function getSubjects()
        {
            $this->db->select('`ID`, `subj_code`, `subj_description`, `subj_units_lec`, `subj_units_lab`');
            $this->db->order_by('`subj_code`', 'ASC');
            $result = $this->db->get('`subject`');
            return $result->result_array();
        }

        public function addSubject($data)
        {
            $this->db->insert('subject', $data);
            return  $this->db->affected_rows() > 0;
        }

        public function editSubject($data,$ID)
        {
            $this->db->where('ID', $ID);
            $this->db->update('subject', $data);
            return  $this->db->affected_rows() > 0;
        }

        public function deleteSubject($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('subject');
            return  $this->db->affected_rows() > 0;
        }

        public function checkDuplicate($data)
        {
            $this->db->where($data);
            $this->db->from('subject');
            $count = $this->db->count_all_results();
            return ($count == 0) ? false:true;
        }

        public function getSubject($ID)
        {
            $this->db->where('ID',$ID);
            $sql = $this->db->get('subject');
            return $sql->result_array();
        }
    }

?>