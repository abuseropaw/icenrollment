<?php
    
    class Room_Model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }
        public function getRooms()
        {
            $result = $this->db->get('`room`');
            return $result->result_array();
        }

        public function getRoom($id)
        {
            $this->db->where('`ID`', $id);
            $query = $this->db->get('`room`');

            return $query->row_array();
        }
        public function addRoom($data)
        {
            $this->db->insert('room', $data);
            return  $this->db->affected_rows() > 0;
        }

        public function editRoom($data,$ID)
        {
            $this->db->where('ID', $ID);
            $this->db->update('room', $data);
            return $this->db->affected_rows() > 0;
        }

        public function deleteRoom($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('room');
            return  $this->db->affected_rows() > 0;
        }

        public function checkDuplicate($data)
        {
            $this->db->where($data);
            $this->db->from('room');
            $count = $this->db->count_all_results();
            return ($count == 0) ? false:true;
        }
    }

?>