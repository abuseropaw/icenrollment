<?php
    
    class Prospectus_Model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }
        

        public function getProspectus($filter)
        {
            $this->db->where($filter);
            $sql = $this->db->get('prospectusDetails');
            return $sql->result_array();
        }

        public function deleteProspectus($data)
        {
            $this->db->where($data);
            $this->db->delete('prospectus');
            return  $this->db->affected_rows() > 0;
        }
        public function editProspectus($data, $filter,$course_id)
        {
            $this->db->where($filter);
            $this->db->update('prospectus', $data);
            return  $this->db->affected_rows() > 0;
        }

        public function editProspectusPrerequisite($data,$filter)
        {
            $this->db->where($filter);
            $this->db->update('prospectus', $data);
            return  $this->db->affected_rows() > 0;
        }


        public function addProspectus($r1,$r2,$r3,$r4,$c_id,$term)
        {
            $this->insert($r1,"First Year",$term,$c_id); 
            $this->insert($r2,"Second Year",$term,$c_id); 
            $this->insert($r3,"Third Year",$term,$c_id); 
            $this->insert($r4,"Forth Year",$term,$c_id); 
            return  $this->db->affected_rows() > 0;
        }
        public function insert($x,$level,$term,$courseID)
        {
            $r = explode(",", $x);
            if(count($r) != 0)
            {
                foreach ($r as $value) {
                    if($value != '')
                    {
                        $data = array(
                            "subj_id" => $value,
                            "course_id" => $courseID,
                            "prospectus_pre_requisites" => null,
                            "prospectus_yearlevel" => $level,
                            "prospectus_term" => $term
                        );
                        if(!$this->duplicate($value,$courseID))
                        {
                            $this->db->insert('prospectus', $data);
                            if($this->db->affected_rows() == 0)
                            {
                                
                            }
                            else {
                                
                            }
                        }
                    }
                }
            }
            return;
        }

        public function duplicate($value,$courseID)
        {
            $newData = array('course_id' => $courseID, 'subj_id' => $value);
            $this->db->where($newData);
            $this->db->from('prospectus');
            $count = $this->db->count_all_results();
            return ($count == 0) ? false:true;
        }
    }

?>
