<?php
    
    class Subjectoffering_Model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }
        public function getSubjectOfferings()
        {
            $this->db->select('*');
            $result = $this->db->get('`subjectofferingdetails`');
            return $result->result_array();
        }


        public function add($data)
        {
            $this->db->insert('subjectoffering', $data);
            return  $this->db->affected_rows() > 0;
        }
        
        public function getSubjectOffering($id)
        {
            $this->db->where('`ID`', $id);
            $query = $this->db->get('`subjectofferingdetails`');
            return $query->row_array();
        }

        public function deleteSubjectOffering($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('subjectoffering');
            return  $this->db->affected_rows() > 0;
        }
    }

?>