<?php
    
    class Student_Model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }
        public function getStudents()
        {
            $this->db->select('*');
            $result = $this->db->get('`student`');
            return $result->result_array();
        }
        public function getStudent($id)
        {
            $this->db->select('*');
            $this->db->where('`ID`', $id);
            $query = $this->db->get('`student`');

            return $query->result_array();
        }
        public function addStudent($data)
        {
            $this->db->insert('student', $data);
            return  $this->db->affected_rows() > 0;
        }
        public function deleteStudent($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('student');
            return  $this->db->affected_rows() > 0;
        }
        public function editStudent($data, $id)
        {
            $this->db->where('ID', $id);
            $this->db->update('student', $data);
            return  $this->db->affected_rows() > 0;
        }
    }
?>