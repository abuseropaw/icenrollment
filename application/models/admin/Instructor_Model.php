<?php
    
    class Instructor_Model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }
        public function getInstructors()
        {
            $this->db->select('*');
            $result = $this->db->get('`instructor`');
            return $result->result_array();
        }
        public function addInstructor($data)
        {
            $this->db->insert('instructor', $data);
            return  $this->db->affected_rows() > 0;
        }
        public function deleteInstructor($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('instructor');
            return  $this->db->affected_rows() > 0;
        }
    }

?>