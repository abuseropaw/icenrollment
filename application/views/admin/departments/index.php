<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
<!-- // MODALS -->
<div class="modal fade" id="editCourse_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method='post' id='form_editCourse'>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span></button>
                    <h4 class="modal-title">Edit Course</h4>
                </div>
                <div class="modal-body">
               
                        <div class='row'>
                        
                            <div class='col-xs-5'>
                                <div class="form-group">
                                    <label for="Gender">Subject</label>
                                    <input class='form-control' name='subject' id='subject' disabled>
                                </div>
                            </div>
                            
                            <div class='col-xs-3'>
                                <div class="form-group">
                                    <label for="Gender">Room</label>
                                    <select class='form-control' name='room' id='room' required>
                                       
                                    </select>
                                </div>
                            </div>
                            <div class='col-xs-4'>
                                <div class="form-group">
                                    <label for="Gender">Instructor</label>
                                    <select class='form-control' name='instructor' id='instructor' required>
                                       
                                    </select>
                                </div>
                            </div>
                            <div class='col-xs-2'>
                                <div class="form-group">
                                    <label for="Gender">Section</label>
                                    <input type='text' name='section' id='section' class='form-control'>
                                </div>
                            </div>    
                            <div class='col-xs-2'>
                                <div class="form-group">
                                    <label for="Gender">Slots</label>
                                    <input type='number' name='slot' id='slot' class='form-control'>
                                    <span class="help-block" id="slotHelp"></span>
                                </div>
                            </div>    
                            <div class='col-xs-2 bootstrap-timepicker'>
                                <div class="form-group">
                                    <label for="Gender">Timein</label>
                                    <input type='text' name='timein' id='timein' class='form-control timepicker'>
                                </div>
                            </div>            
                            <div class='col-xs-2 bootstrap-timepicker'>
                                <div class="form-group">
                                    <label for="Gender">Timeout</label>
                                    <input type='text' name='timeout' id='timeout' class='form-control timepicker'>
                                </div>
                            </div>
                            <div class='col-xs-4'>
                                <div class="form-group">
                                    <label for="Gender">Days</label><br />
                                    <label>
                                    <input type="checkbox" name="day[]" class="flat-red" value="M"> 
                                    </label> Monday &nbsp;
                                    <label>
                                    <input type="checkbox" name="day[]" class="flat-red" value="TH"> 
                                    </label> Tuesday &nbsp;
                                    <label>
                                    <input type="checkbox" name="day[]" class="flat-red" value="W"> 
                                    </label> Wednesday &nbsp;
                                    <label>
                                    <input type="checkbox" name="day[]" class="flat-red" value="TH"> 
                                    </label> Thursday &nbsp;
                                    <label>
                                    <input type="checkbox" name="day[]" class="flat-red" value="F"> 
                                    </label> Friday &nbsp;
                                    <label>
                                    <input type="checkbox" name="day[]" class="flat-red" value="SAT"> 
                                    </label> Saturday &nbsp;
                                </div>           
                            </div>            
                        </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning pull-left" data-dismiss="modal"><i class='fa fa-close'></i> Close</button>
                    <button type="submit" class="btn btn-success"><i class='fa fa-check'></i> Save course</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- // MODALS -->
    <section class="content-header">
        <h1><?php echo $pagetitle; ?></h1>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <!--LEFT DIV  -->
            <div class="col-md-3">
                
                <div class="box box-success box-solid">
                    <div class="box-header ">
                    <h3 class="box-title">New Department</h3>
                    </div>
                    <?php echo form_open('', array("id" => "form_addDepartment")); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="code">Description</label>
                            <input type='text' class='form-control' id='description' name='description' placeholder='eg. DIT'>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <div class='pull-right'>
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Submit</button> &nbsp;
                            <button type="reset" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    
                </div>
                  
            </div>
            <!--RIGHT DIV  -->
            <div class="col-md-9">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                    <h3 class="box-title">List of Departments</h3>

                    <div class="box-tools">
                        
                        <div class="input-group input-group-sm" style="width: 200px;height: 27px;">
                        
                            <input type="text" id="table_search" class="form-control pull-right" placeholder="Search student">
                            
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    </div>
                    <!-- /.box-header -->
                        <div class="box-body table-responsive" id='departmentTable'>
                            
                        </div>
                    </div>
            </div>
        </div>
    </section>
</div>


<script>
    $(function(){
        // AJAX DISPLAY
        displaySubjectOfferings();

        function displaySubjectOfferings()
        {
            LoadDataFromUrlToTable('subjectofferings/getAll', 'subjectofferingTable')
        }

        $(document).on('submit', '#form_addDepartment', function(e){
            e.preventDefault();
            PostDataToUrl($(this).serialize(), 'departments/add').then(function(data){
                switch (data.content.status) {
                    case 'duplicate':
                        toastr.error(data.content.message);
                        break;
                    case 'ok':
                        $('#form_addCourse')[0].reset();
                        toastr.success(data.content.message);
                        displayCourse();
                        break;
                    default:
                        break;
                }
            });
        })
    });
</script>