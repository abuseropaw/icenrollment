<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper" style="min-height: 1126px;">
    
<section class="content-header">
        <h1><?php echo "Profile"; ?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="http://127.0.0.1/ICEnrollment/admin/dashboard">
                    <i class="fa fa-dashboard"></i> Dashboard
                </a> 
            </li>
            <li>
                <a href="http://127.0.0.1/ICEnrollment/admin/courses">
                    <i class="fa fa-dashboard"></i> Student
                </a> 
            </li>
            <li class="active">Profile

            </li>
        </ol>
      
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
              <h3 class="profile-username text-center">Nina Mcintire</h3>
              <p class="text-muted text-center">Software Engineer</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Malibu, California</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class='active'><a href="#editProfile" data-toggle="tab">Edit Profile</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="editProfile">
              <?php echo form_open('', array("id" => "form_editStudent")); ?>
              <div class="box-body">
                  <div class='row'>
                    <div class='col-xs-3'>
                      <div class="form-group">
                        <label for="student_id">ID</label>
                        <input class='form-control' type='text' id='student_id' name='student_id' value='' ></input>
                      </div>
                    </div>
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_fname">First name</label>
                              <input class='form-control' type='text' id='student_fname' name='student_fname' value='' ></input>
                          </div>
                      </div>
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_mname">Middle name</label>
                              <input class='form-control' type='text' id='student_mname' name='student_mname' value='' ></input>
                          </div>
                      </div>
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_lname">Last name</label>
                              <input class='form-control' type='text' id='student_lname' name='student_lname' value='' ></input>
                          </div>
                      </div>
                 
                  
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_gender">Gender</label>
                              <select class='form-control' name='student_gender' id='student_gender' required>
                                  <option value='' disabled selected>-- Select gender --</option>
                                  <option>Male</option>
                                  <option>Female</option>
                              </select>
                          </div>
                      </div>
                      
                      <div class='col-xs-4'>
                          <div class="form-group">
                              <label for="student_bplace">Birth place</label>
                              <input type='text' name='student_bplace' id='student_bplace' class='form-control' placeholder="Birth place">
                          </div> 
                      </div>
                      
                      <div class='col-xs-2'>
                          <div class="form-group date">
                              <label for="student_bdate">Birth date</label>
                               <input type='text' name='student_bdate' class='form-control' id='student_bdate' placeholder="Birth date">
                          </div>
                      </div>
                      <div class='col-xs-1'>
                          <div class="form-group">
                              <label for="age">Age</label>
                              <input type='text' name='age' class='form-control' id='age' placeholder="Age" disabled>
                          </div>
                      </div>
                      <div class='col-xs-2'>
                          <div class="form-group">
                              <label for="student_religion">Religion</label>
                              <input type='text' name='student_religion' id='student_religion' class='form-control' placeholder="Religion">
                          </div> 
                      </div>
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_address_street">Street #</label>
                              <input type='text' name='student_address_street' id='student_address_street' class='form-control' placeholder="Street # of student address">
                          </div>
                      </div>
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_address_barangay">Barangay</label>
                              <input type='text' name='student_address_barangay' id='student_address_barangay' class='form-control' placeholder="Barangay of student address">
                          </div>
                      </div>
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_address_municipality">Municipality</label>
                              <input type='text' name='student_address_municipality' id='student_address_municipality' class='form-control' placeholder="Municipality of student address">
                          </div>
                      </div>
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_address_province">Province</label>
                              <input type='text' name='student_address_province' id='student_address_province' class='form-control' placeholder="Province of student address">
                          </div>
                      </div>
                      
                      <div class='col-xs-3'>
                          <div class="form-group">
                              <label for="student_status">Civil status</label>
                              <select class='form-control' name='student_status' id='student_status' required>
                                  <option value='' disabled selected>-- Select status --</option>
                                  <option>Single</option>
                                  <option>Married</option>
                                  
                              </select>
                          </div>
                      </div>
                      <div class='col-xs-4'>
                          <div class="form-group">
                              <label for="student_spouse_name">Spouse name (Optional)</label>
                              <input type='text' name='student_spouse_name' id='student_spouse_name' class='form-control' placeholder="Name of spouse if applicable">
                          </div>
                      </div>
                      <div class='col-xs-5'>
                          <div class="form-group">
                              <label for="student_last_school_year_attended">Last school attended</label>
                              <input type='text' name='student_last_school_year_attended' id='student_last_school_year_attended' class='form-control' placeholder="Last school attended">
                          </div>
                      </div>
                  </div>
                  <hr>
                  <div class='row'>
                      <div class='col-xs-6'>
                          <div class="form-group">
                              <label for="student_fathers_name">Father's name</label>
                              <input type='text' name='student_fathers_name' id='student_fathers_name' class='form-control' placeholder="Father's full name">
                          </div>
                      </div>
                      <div class='col-xs-6'>
                          <div class="form-group">
                              <label for="student_fathers_occupation">Father's occupation</label>
                              <input type='text' name='student_fathers_occupation' id='student_fathers_occupation' class='form-control' placeholder="Father's occupation">
                          </div>
                      </div>
                      <div class='col-xs-6'>
                          <div class="form-group">
                              <label for="student_mothers_name">Mother's name</label>
                              <input type='text' name='student_mothers_name' id='student_mothers_name' class='form-control' placeholder="Mother's full name">
                          </div>
                      </div>
                      <div class='col-xs-6'>
                          <div class="form-group">
                              <label for="student_mothers_occupation">Mother's occupation</label>
                              <input type='text' name='student_mothers_occupation' id='student_mothers_occupation' class='form-control' placeholder="Mother's occupation">
                          </div>
                      </div>
                      <div class='col-xs-6'>
                          <div class="form-group">
                              <label for="student_contact_number">Contact number</label>
                              <input type='text' name='student_contact_number' id='student_contact_number' class='form-control' placeholder="Contact number">
                          </div>
                      </div>
                      <div class='col-xs-6'>
                          <div class="form-group">
                              <label for="student_guardian">Guardian's name</label>
                              <input type='text' name='student_guardian' id='student_guardian' class='form-control' placeholder="Guardian's full name">
                          </div>
                      </div>
                  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                  <div class='pull-right'>
                      <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save Changes</button> &nbsp;
                      <button type="button" onclick="populateDataToForm();" class="btn btn-info"><i class="fa fa-refresh"></i> Reset</button>
                  </div>
              </div>
              <?php echo form_close(); ?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

  <script>
  $('#student_bdate').datepicker({ autoclose: true });

  //POPULATE data to form_editStudent
  $(document).ready(function(){
    populateDataToForm();
  });

  function populateDataToForm()
  {
    var id = <?php echo $id ?>;
    $.ajax({
      url: '<?=base_url('admin/students/getStudent')?>',
      method: 'get',
      data: {id:id},
      success: function(data){
        var data = $.parseJSON(data);
        $('#student_id').val(data[0].student_id);
        $('#student_fname').val(data[0].student_fname);
        $('#student_mname').val(data[0].student_mname);
        $('#student_lname').val(data[0].student_lname);
        $('#student_gender').val(data[0].student_gender);
        $('#student_bplace').val(data[0].student_bplace);
        $('#student_bdate').val((data[0].student_bdate).split('-')[1]+'/'+(data[0].student_bdate).split('-')[2]+'/'+(data[0].student_bdate).split('-')[0]);
        $('#student_religion').val(data[0].student_religion);
        $('#student_address_street').val(data[0].student_address_street);
        $('#student_address_barangay').val(data[0].student_address_barangay);
        $('#student_address_municipality').val(data[0].student_address_municipality);
        $('#student_address_province').val(data[0].student_address_province);
        $('#student_status').val(data[0].student_status);
        $('#student_spouse_name').val(data[0].student_spouse_name);
        $('#student_last_school_year_attended').val(data[0].student_last_school_year_attended);
        $('#student_fathers_name').val(data[0].student_fathers_name);
        $('#student_fathers_occupation').val(data[0].student_fathers_occupation);
        $('#student_mothers_name').val(data[0].student_mothers_name);
        $('#student_mothers_occupation').val(data[0].student_mothers_occupation);
        $('#student_contact_number').val(data[0].student_contact_number);
        $('#student_guardian').val(data[0].student_guardian);
      }
    });
  }

  $('#form_editStudent').on('submit', function(e){
      e.preventDefault();
      console.log($(this).serialize());
      PutDataToUrl($(this).serialize(), '../../students/edit/?id='+<?php echo $id; ?>).then(function(data){
          toastr.success('Student profile saved!');
          populateDataToForm();
      });
  });
  </script>