<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div class="content-wrapper">
    <section class="content-header">
        <h1><?php echo $pagetitle; ?></h1>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <!--LEFT DIV  -->
            <div class="col-md-3">
                
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                    <h3 class="box-title">New Instructor</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php echo form_open('', array("id" => "form_addInstructor")); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="Name">Name</label>
                            <?php echo form_input($instructor_name);?>
                        </div>
                        <div class="form-group">
                            <label for="Address">Address</label>
                            <?php echo form_input($instructor_address);?>
                        </div>
                        <div class="form-group">
                            <label for="Position">Position</label>
                            <?php echo form_input($instructor_position);?>
                        </div>
                        <div class="form-group">
                            <label for="Gender">Gender</label>
                            <select class='form-control' name='instructor_gender' required>
                                <option value=''>-- Select gender --</option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Gender">Civil Status</label>
                            <select class='form-control' name='instructor_civil_status' required>
                                <option value=''>-- Select civil status --</option>
                                <option>Single</option>
                                <option>Married</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Position">Email</label>
                            <?php echo form_input($instructor_email_address);?>
                        </div>
                        <div class="form-group">
                            <label for="Position">Specialization</label>
                            <?php echo form_input($instructor_specialization);?>
                        </div>
                        <div class="form-group">
                            <label for="Gender">Employment Status</label>
                            <select class='form-control' name='instructor_employment_status' required>
                                <option value=''>-- Select employment status --</option>
                                <option>Active</option>
                                <option>Resigned</option>
                                <option>Retired</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <div class='pull-right'>
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button> &nbsp;
                            <button type="reset" class="btn btn-info"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    
                </div>
                  
            </div>
            <!--RIGHT DIV  -->
            <div class="col-md-9">
                <div class="box box-primary box-solid">
                    <div class="box-header">
                    <h3 class="box-title">List of Instructor</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 200px;height: 27px;">
                        <input type="text" id="table_search" class="form-control pull-right" placeholder="Search instructor">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive" id="instructorTable"></div>
                </div>
            </div>
        </div>
    </section>
</div>
 
 <script src="<?php echo base_url($plugins_dir . '/bootstrap-confirmation/bootstrap-confirmation.min.js'); ?>"></script>
 <script src="<?php echo base_url($plugins_dir . '/bootstrap-confirmation/bootstrap-confirmation.js'); ?>"></script>
<script>
    $(function(){
        $('#overlay').html('<i class="fa fa-refresh fa-spin"></i>');
        
        // AJAX DISPLAY
        displayInstructor();

        function displayInstructor()
        {
            LoadDataFromUrlToTable('instructors/getAll', 'instructorTable');
        }

        $('#form_addInstructor').on('submit', function(e){
            e.preventDefault();

            $.ajax({
                url: '<?=base_url('admin/instructors/add')?>',
                data: $(this).serialize(),
                type: 'post',
                success: function(data)
                {
                    toastr.success('Instructor successfully added!');
                    displayInstructor();
                }
            });
        });

        $(document).on('mouseover', 'a[class^="btn"]', function(e){
            e.preventDefault();
            let id = ($(this)[0].id).split('_')[1];
            $('#'+$(this)[0].id+'').confirmation({
                onConfirm: function(){
                    DeleteDataFromUrl({id:id}, 'instructors/delete').then(function(data){
                        toastr.success(data.content.message);
                        displayInstructor();
                    })
                },
                onCancel: function(){}
            });  
        });

        $('#table_search').keyup(function(){
            var txt = $('#table_search').val();
            var filter, table, tr, td, i;
            filter = txt.toUpperCase();
            table = document.getElementById("instructorTable");
            tr = table.getElementsByTagName("tr");
            console.log(tr);
            for (i = 0; i < tr.length; i++) {
                td1 = tr[i].getElementsByTagName("td")[0];
                td2 = tr[i].getElementsByTagName("td")[1];
                td3 = tr[i].getElementsByTagName("td")[2];
                td4 = tr[i].getElementsByTagName("td")[3];
                td5 = tr[i].getElementsByTagName("td")[4];
                td6 = tr[i].getElementsByTagName("td")[5];
                td7 = tr[i].getElementsByTagName("td")[6];
                td8 = tr[i].getElementsByTagName("td")[7];
                td9 = tr[i].getElementsByTagName("td")[8];
                
                
                if (td1) {
                    if (td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 ||
                     td3.innerHTML.toUpperCase().indexOf(filter) > -1 || td4.innerHTML.toUpperCase().indexOf(filter) > -1 || td5.innerHTML.toUpperCase().indexOf(filter) > -1
                      || td6.innerHTML.toUpperCase().indexOf(filter) > -1 || td7.innerHTML.toUpperCase().indexOf(filter) > -1 || td8.innerHTML.toUpperCase().indexOf(filter) > -1
                       || td9.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }   
                
            }
            
        });
    });
</script>