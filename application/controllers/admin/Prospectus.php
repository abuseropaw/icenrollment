<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Prospectus extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        /* Load :: Common */
        //$this->lang->load('admin/courses');

        $this->load->model('admin/prospectus_model');
        
        $this->load->library('session');
        $this->page_title->push(lang('menu_courses'));
        $this->data['pagetitle'] = 'Prospectus';

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Courses', 'admin/courses');
    }
    public function add()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $r1 = $this->is_valid_post('r1');
            $r2 =$this->is_valid_post('r2');
            $r3 = $this->is_valid_post('r3');
            $r4 = $this->is_valid_post('r4');
            $c_id = $this->is_valid_post('c_id');
            $term = $this->is_valid_post('term');

            if($this->prospectus_model->addProspectus($r1,$r2,$r3,$r4,$c_id,$term))
            {
                $sem = 'Successfully added subject to '. $term .'';
                $result = array('status' => 'ok', 'message' => $sem);
                echo json_encode($result);
            }
        }
    }
    public function editPrerequisite()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $subj_id = $this->is_valid_post('subj_id');
            $course_id = $this->is_valid_post('course_id');
            $subjID = $this->is_valid_post('subjID');
            if($subj_id && $course_id && $subjID)
            {
                $data = array(
                    "prospectus_pre_requisites" => $subjID 
                );
                $filter = array(
                    'subj_id' => $subj_id,
                    'course_id' => $course_id 
                );
                if($this->prospectus_model->editProspectusPrerequisite($data,$filter))
                {
                    $data = array('status' => 'ok', 'message' => 'Successfully updated prerequisite');
                    echo json_encode($data);
                }
            }
        }
    }

    public function delete()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $id = $this->is_valid_get('id');
            if($id)
            {
                $tempID = explode("_",$id);
                $subj_id = $tempID[0];
                $course_id = $tempID[1];
                
                $data = array('subj_id' => $subj_id, 'course_id' => $course_id);
                if($this->prospectus_model->deleteProspectus($data))
                {
                    $data = array('status' => 'ok', 'message' => 'Successfully deleted subject');
                    echo json_encode($data);
                }
            }
        }
    }
    public function edit()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $subj_id = $this->is_valid_post('subjID');
            $course_id = $this->is_valid_post('courseID');
            $prospectus_yearlevel = $this->is_valid_post('level');
            $prospectus_term = $this->is_valid_post('term');
            $old = $this->is_valid_post('old');
            if($subj_id && $course_id && $prospectus_yearlevel && $prospectus_term)
            {
                $data = array(
                    "subj_id" => $subj_id
                );
                $filter = array(
                    "subj_id" => $old,
                    "course_id" => $course_id,
                    "prospectus_yearlevel" => $prospectus_yearlevel,
                    "prospectus_term" => $prospectus_term
                );
                if($this->prospectus_model->editProspectus($data,$filter,$course_id))
                {
                    $data = array('status' => 'ok', 'message' => 'Successfully updated prospectos');
                    echo json_encode($data);
                }
                else {
                    $data = array('status' => 'duplicate', 'message' => 'Subject already exist');
                    echo json_encode($data);
                }
            }
        }
    }

    public function getProspectus()
    {
        $id = $this->input->get('id');
        $filter = array('course_id' => $id);
        $data = $this->prospectus_model->getProspectus($filter);
        echo json_encode($data);
        return;
    }

    public function getAll()
    {
        $id = $this->input->get('id');
        $level = array('First Year','Second Year','Third Year','Forth Year');
        $term = array('First Semester', 'Second Semester', 'Summer');
        echo "
        <table id='courseTable' class='table table-bordered tftable' role='grid'> 
                
            <tr class='tr'> 
                <th class='prosTH'>Subject Code </th>
                <th class='prosTH'>Descriptive Title</th>
                <th class='prosTH'>Units(Lec)</th>
                <th class='prosTH'>Units(Lab)</th>
                <th class='prosTH'>Units(Total Units)</th>
                <th class='prosTH'>Pre Requisite</th>

            </tr>
 
        ";
        foreach ($level as $lvl => $value_lvl) {
            foreach ($term as $trm => $value_term) {
                echo "
            <tr class='tr'>
                <td colspan='100'><b><center>".$value_lvl." - ".$value_term."</center></b></td>
            </tr>
                ";
                $filter = array(
                    'course_id' => $id,
                    'prospectus_yearlevel' => $value_lvl, 
                    'prospectus_term' => $value_term
                );
                $specificProspectus = $this->prospectus_model->getProspectus($filter);
                foreach ($specificProspectus as $sp => $value_sp) {
                    $totalUnits = $value_sp['subj_units_lec'] + $value_sp['subj_units_lab'];
                    echo "
            <tr class='tr'><center>
                <td>".$value_sp['subj_code']."</td>
                <td>".$value_sp['subj_description']."</td>
                <td>".$value_sp['subj_units_lec']."</td>
                <td>".$value_sp['subj_units_lab']."</td>
                <td>".$totalUnits."</td>
                <td>".$value_sp['Prerequisite']."</td>
                </center>
            </tr>
                    ";
                }
            }
        }

        echo "
        </table>
        ";
    }

















    public function is_valid_post($variable)
    {
        if (!empty($this->input->post($variable)))
        {   
            return $this->input->post($variable);
        }
        else
        {
            return false;
        }
    }
    public function is_valid_get($variable)
    {
        if (!empty($this->input->get($variable)))
        {   
            return $this->input->get($variable);
        }
        else
        {
            return false;
        }
    }
    
}

?>