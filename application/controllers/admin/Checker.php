<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Courses extends Admin_Controller
{
    public function is_valid_post($variable)
    {
        if (!empty($this->input->post($variable)))
        {   
            return $this->input->post($variable);
        }
        else
        {
            return false;
        }
    }
}