<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Departments extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        /* Load :: Common */
        //$this->lang->load('admin/departments');
        // $this->load->model('admin/prospectus_model');
        $this->load->library('session');
        $this->page_title->push(lang('menu_departments'));
        $this->data['pagetitle'] = 'Departments';

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, 'Departments', 'admin/departments');
    }

    public function index()
    {
        // $this->data['courses'] = $this->course_model->getCourses(); // Data nga gi pass
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
        $this->template->admin_render('admin/departments/index', $this->data);
    }

    

    public function is_valid_post($variable)
    {
        if (!empty($this->input->post($variable)))
        {   
            return $this->input->post($variable);
        }
        else
        {
            return false;
        }
    }
    public function is_valid_get($variable)
    {
        if (!empty($this->input->get($variable)))
        {   
            return $this->input->get($variable);
        }
        else
        {
            return false;
        }
    }
    
}

?>