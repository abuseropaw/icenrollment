

create view subjectofferingdetails as
select so.ID, so.academicyear_id, so.instructor_id, so.subj_id, so.room_id,
    so.subjectoffering_slots,
    so.subjectoffering_section,
    so.subjectoffering_timein,
    so.subjectoffering_timeout,
    so.subjectoffering_days,
    so.subjectoffering_semester,
    so.subjectoffering_level,
    so.subjectoffering_status,
    i.instructor_name,
    i.instructor_address,
    i.instructor_position,
    i.instructor_gender,
    i.instructor_civil_status,
    i.instructor_email_address,
    i.instructor_specialization,
    i.instructor_employment_status,
    s.subj_code,
    s.subj_description,
    s.subj_units_lec,
    s.subj_units_lab,
    s.subj_prerequisite,
    r.room_building_name,
    r.room_number,
    r.room_capacity
    from subjectoffering so
    inner join instructor i on so.instructor_id = i.ID
    inner join subject s on so.subj_id = s.ID
    inner join room r on so.room_id = r.ID;